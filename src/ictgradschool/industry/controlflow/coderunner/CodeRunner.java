package ictgradschool.industry.controlflow.coderunner;

/**
 * Please run TestCodeRunner to check your answers
 */
public class CodeRunner {
    /**
     * Q1. Compare two names and if they are the same return "Same name",
     * otherwise if they start with exactly the same letter return "Same
     * first letter", otherwise return "No match".
     *
     * @param firstName
     * @param secondName
     * @return one of three Strings: "Same name", "Same first letter",
     * "No match"
     */
    public String areSameName(String firstName, String secondName) {
        String message = " ";
        // TODO write answer to Q1
        if(firstName.equals(secondName)){
            message = "Same name";
        }
        else if (firstName.charAt(0)==secondName.charAt(0)){
            message ="Same first letter";
        }
        else{
            message="No match";
        }


        return message;
    }
    /** areSameName(String, String) => String **/


    /**
     * Q2. Determine if the given year is a leap year.
     *
     * @param year
     * @return true if the given year is a leap year, false otherwise
     */
    public boolean isALeapYear(int year) {

        // TODO write answer for Q2

        boolean leapYear;

        if (year % 4 == 0 && !(year % 100 == 0 && year % 400 != 0)){
            leapYear = true;
        }
        else{
            leapYear = false;
        }


        return leapYear;
    }
    /** isALeapYear(int) => boolean **/


    /**
     * Q3. When given an integer, return an integer that is the reverse (its
     * numbers are in reverse to the input).
     * order.
     *
     * @param number
     * @return the integer with digits in reverse order
     */
    public int reverseInt(int number) {

        int reverseNum = 0;
        boolean isNegative = false;
        if (number < 0) {
            isNegative = true;
            number = number * -1;
        }
        while(number>0){
            int remainder = number %10;
            reverseNum = reverseNum *10 +remainder;
            number =number/10;
        }

        if (isNegative) {
            reverseNum = reverseNum * -1;
        }

        // TODO write answer for Q3
        return reverseNum;
    }
    /** reverseInt(int) => void **/


    /**
     * Q4. Return the given string in reverse order.
     *
     * @param str
     * @return the String with characters in reverse order
     */
    public String reverseString(String str) {
        // TODO write answer for Q4
        String reversed ="";
        for(int i=0; i<str.length(); i++){
            reversed= str.charAt(i)+reversed;
        }
        return reversed;
    }
    /** reverseString(String) => void **/
//          String reverse = "";
//        for(int i=str.length()-1;i>=0;i--){
//            reverse= reverse + str.charAt(i);
//        }


    /**
     * Q5. Generates the simple multiplication table for the given integer.
     * The resulting table should be 'num' columns wide and 'num' rows tall.
     *
     * @param num
     * @return the multiplication table as a newline separated String
     */
    public String simpleMultiplicationTable(int num) {
        String multiplicationTable = "";
        for(int i=1; i<=num; i++){
            for(int j=1; j<=num; j++){
                multiplicationTable += i*j+" ";

            }
            multiplicationTable=multiplicationTable.trim();
            if(i != num){
                multiplicationTable += "\n";
            }
        }
        // TODO write answer for Q5

        return multiplicationTable;
    }
    /** simpleMultiplicationTable(int) => void **/


    /**
     * Q6. Determines the Excel column name of the given column number.
     *
     * @param num
     * @return the column title as a String
     */
    public String convertIntToColTitle(int num) {
        String columnName = "";
        // TODO write answer for Q6
        if(num >0 ){
            columnName = columnName + (char)((num-1)%26 +65);
            while ((num/27) !=0 ){
                num=num/26;
                columnName = (char)((num%26 +64)) +columnName;
            }
        } else {
                columnName= "Input is invalid";

        }
        return columnName;
    }
    /** convertIntToColTitle(int) => void **/


    /**
     * Q7. Determine if the given number is a prime number.
     *
     * @param num
     * @return true is the given number is a prime, false otherwise
     */
    public boolean isPrime(int num) {
        // TODO write answer for Q7
        boolean isprime = true;
        int i =2;
        while (num % i !=0 && i <= num /2){
            i++;
        }
        if (i ==num /2+1){
            return true;
        }else {
            return false;
        }
    }
    /** isPrime(int) => void **/


    /**
     * Q8. Determine if the given integer is a palindrome (ignoring negative
     * sign).
     *
     * @param num
     * @return true is int is palindrome, false otherwise
     */
    public boolean isIntPalindrome(int num) {
        // TODO write answer for Q8
        if(num==reverseInt(num)){
            return true;
        }
        return false;
    }
    /** isIntPalindrom(int) => boolean **/


    /**
     * Q9. Determine if the given string is a palindrome (case folded).
     *
     * @param str
     * @return true if string is palindrome, false otherwise
     */
    public boolean isStringPalindrome(String str) {
        // TODO write answer for Q9
        String reversedStr ="";
        str = str.replaceAll(" ", "");
        for(int i=0; i<str.length(); i++){
            reversedStr = str.charAt(i)+reversedStr;
        }
        if(str.equals(reversedStr)){
            return true;
        }else{
            return false;
        }

    }
    /** isStringPalindrome(String) => boolean **/


    /**
     * Q10. Generate a space separated list of all the prime numbers from 2
     * to the highest prime less than or equal to the given integer.
     *
     * @param num
     * @return the primes as a space separated list
     */
    public String printPrimeNumbers(int num) {
        String primesStr = "";
        // TODO write answer for Q10
        for(int i= 2; i<= num ; i++){
            if(isPrime(i)){
                primesStr = primesStr + i + " ";
            }
        }

        if (!primesStr.isEmpty()){
            return primesStr.trim();
        }else {
            return "No prime number found";
        }
    }
}
