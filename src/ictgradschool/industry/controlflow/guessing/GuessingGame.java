package ictgradschool.industry.controlflow.guessing;

import ictgradschool.Keyboard;

import java.security.Key;

/**
 * A guessing game!
 */

public class GuessingGame {

    public void start() {

        int randomNumber=(int)(Math.random()*(100)) + 1;
        int guess=0;
        while(guess != randomNumber) {
            System.out.println("Enter your guess");
            guess=Integer.parseInt(Keyboard.readInput());
            if(guess>randomNumber){
                System.out.println("Too high");
            }else if (guess<randomNumber){
                System.out.println("Too low");
            }else {
                System.out.println("perfect");
            }

        }
        System.out.println("goodbye");


        // TODO Write your code here.
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
